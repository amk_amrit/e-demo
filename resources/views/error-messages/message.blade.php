@if(session('flash_message_success'))
    <div class="alert alert-success " role="alert">
        <p class="custom-success-message">{{session('flash_message_success')}}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="custom-success-message" aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session('flash_message_danger'))
    <div class="alert alert-danger " role="alert">
        {{session('flash_message_danger')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(session('flash_message_error'))
    <div class="alert alert-danger" role="alert">
        {{session('flash_message_error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if (count($errors) > 0)

    @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
           {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
@endif
{{--@php($oldFields = session('extraErrorFields'))--}}
{{--@if(isset($oldFields) && count($oldFields) > 0)--}}
{{--   {{dd($oldFields)}}--}}
{{--@endif--}}
