@extends('frontend.layouts.app')

@section('content')
    <!--================login_part Area =================-->
    <section class="login_part ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                            <h3>Welcome  ! <br>
                                Please Sign up now</h3>
                            <form class="row contact_form" action="{{route('customer.sign-up')}}" method="post" >
                                @csrf
                            <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control"  name="name" placeholder="Name">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="address" placeholder="Address">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="facebook_id" placeholder="Facebook Id">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="google_id" placeholder="Google Id">
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone Number">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="mobile_number" placeholder="Mobile Number">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="col-md-12 form-group">
                                    <button type="submit" value="submit" class="btn_3">
                                        Sign Up
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
    @endsection