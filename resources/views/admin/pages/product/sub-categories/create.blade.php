@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">
						<div class="module">
							<div class="module-head">
								<h3>Create</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.product.sub-categories.store')}}" method="POST" enctype="multipart/form-data" >
											{{ csrf_field() }}	

											<div class="control-group">
											<label class="control-label" for="basicinput">Category</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="product_category_id">
													<option value="" disabled selected>Select here..</option>
													@foreach($productCategories as $productCategory)
													<option value="{{$productCategory->id}}" {{ (old ('product_category_id')==$productCategory->id)? "selected" : ''}} >{{$productCategory->product_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>									
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Sub Category Name</label>
											<div class="controls">
												<input type="text" name="product_sub_category_name" id="basicinput" placeholder="Type Category Name here..." class="span8" value="{{old('product_sub_category_name')}}">
											</div>
                                        </div>											
										<div class="control-group">
											<label class="control-label">Status</label>
											<div class="controls">
												<label class="radio">
													<input type="radio" name="status" id="optionsRadios1" value="1" {{(old('status')=='1') ? 'checked' : ''}}>
													Active
												</label> 
												<label class="radio">
													<input type="radio" name="status" id="optionsRadios2" value="0" {{(old('status')=='0') ? 'checked' : ''}}>
													Deactive
												</label> 
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{Request::old('description')}}</textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Create  </button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection