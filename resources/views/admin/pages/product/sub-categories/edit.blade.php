@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Update</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.product.sub-categories.update', $productSubCategory->id)}}" method="POST" enctype="multipart/form-data" >
                                            {{ csrf_field() }}	
											<input type="hidden" name="_method" value="PUT">
											

											<div class="control-group">
											<label class="control-label" for="basicinput">Category</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="product_category_id">
													<option value="">Select here..</option>
													@foreach($productCategories as $productCategory)
													<option value="{{$productCategory->id}}" {{($productSubCategory->product_category_id == $productCategory->id) ? 'selected' : ''}}>{{$productCategory->product_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>			
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Category Name</label>
											<div class="controls">
												<input type="text" name="product_sub_category_name" id="basicinput" placeholder="Type Category Name here..." class="span8" value="{{$productSubCategory->product_sub_category_name}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Status</label>
											<div class="controls">
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultChecked" name="status"  value="1" {{$productSubCategory->status == 1 ? 'checked' : '' }} >
													Active
												</label> 
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$productSubCategory->status == 0 ? 'checked' : '' }} >
													Deactive
												</label> 
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{$productSubCategory->description}}</textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Update </button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection