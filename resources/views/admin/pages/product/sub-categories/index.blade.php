@extends('admin.layouts.app')
@section('title','Product Sub Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<a href="{{route('admin.product.sub-categories.create')}}" class="btn btn-info">Create New Sub Product Category</a>
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Product Sub Category list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>Sub Category Name</th>
											<th>Category Name</th>
											<th>Description</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($productSubCategories as $productSubCategory)
										<tr class="odd gradeX">
											<td>{{ $productSubCategory->product_sub_category_name }}</td>
											<td>{{$productSubCategory->productCategory->product_category_name}}</td>
											<td>{{$productSubCategory->description}}</td>
											<td>
												@if($productSubCategory->status==1)
												Active
												@else
												Deactivate
												@endif
											</td>
											<td>
												<a href="{{route('admin.product.sub-categories.edit', $productSubCategory->id)}}" class="btn btn-info">Edit</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection