@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')
@include('error-messages.message')

<table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Product Attribute  </th>
									</tr>
								  </thead>
								  <tbody>
                          <form action="{{route('admin.product.images.update', $productId)}}" method="POST" enctype="multipart/form-data">
						  {{ csrf_field() }}
						  <input type="hidden" name="_method" value="PUT">
									<tr>								 
									   <td>
										  <select name="product_attribute_id[]">
											  @foreach($productAttributes as $productAttribute )
											  <option value="{{$productAttribute->id}}" >{{$productAttribute->color}}</option>
											  @endforeach
										  </select>
									  </td>
									  <td><input type="file"  name="productImage[]" required></td>
                           </tr>
								  </tbody>
                        </table>                        
                        <div class="form-group">
                        <input type="submit" id="submitted"  class="btn btn-success" value="create">
                        </div>

                </form>
							</div>
                  </div>
                  @endsection