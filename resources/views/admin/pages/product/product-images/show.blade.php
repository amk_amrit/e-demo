@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')
<div>
        <h5 style="text-align: center;">Product Images</h5>
	</div>
	<div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-body">
                                @if($productImages->isEmpty())
                                No Record Found
                                @endif
								@foreach($productImages as $productImage)

                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="media user">
                                        <?php $url=url('productImage/'.$productImage->images); 
                                				?>
                                            <a class="media-avatar pull-left" href="{{ asset('/productImage/' .$productImage->images)}}">
                                                <img src="{{$url}}">
                                            </a>
                                            <div class="media-body">
                                                <table class="table" align="center" style="width: 70%;">
                                                        <tr>
                                                            <th>Size</th>
                                                            <td>{{$productImage->productImageAttribute->size}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Color</th>
                                                            <td>{{$productImage->productImageAttribute->color}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Price</th>
                                                            <td>Rs.{{$productImage->productImageAttribute->price}}</td>
														</tr>
														<tr>
                                                            <th>Action</th>
                                                            <td>
															{!! Form::open(['route' => ['admin.product.images.destroy', $productImage->id], 'method' => 'delete']) !!}

															{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

															{!! Form::close() !!}
															</td>
                                                        </tr>
                                                    </table>
											</div>
                                        </div>
									</div>
									
								</div>

                                <!--/.row-fluid-->
								<br />
								@endforeach

							</div>

                        </div>
					</div>

                    <!--/.content-->
				</div>

                <!--/.span9-->
                        @endsection