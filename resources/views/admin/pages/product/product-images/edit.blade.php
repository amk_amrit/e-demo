@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')
@include('error-messages.message')

<table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Product Attribute  </th>
									  <th>Image</th>
									</tr>
								  </thead>
								  <tbody>
                          <form action="{{route('admin.product.images.store')}}" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}	
                             @foreach($productImages as $productImage)
									<tr>
									  <td>{{$loop->iteration}}</td>
									  <input type="hidden" value="{{$productImage->id}}" name="id[]">
									  <input type="hidden" value="{{$productImage->product_id}}" name="product_id[]">
									  <td>
										  <select name="product_attribute_id[]">
											  @foreach($productAttributes as $productAttribute )
											  <option value="{{$productAttribute->id}}" {{($productAttribute->id == $productImage->product_attribute_id) ? 'selected' : ''}}>{{$productAttribute->color}}</option>
											  @endforeach
										  </select>
									  </td>
									  <?php $url=url('productImage/'.$productImage->images); 
                                				?>
									  <td><input type="file" value="{{$productImage->images}}" name="productImage[]"></td>
									  <td>
									  <a href="{{ asset('/productImage/' .$productImage->images)}}"><img class="d-block w-100" src="{{$url}}"  alt="Snow" height="100px" width="100px"></a>
									  </td>
                           </tr>
                           @endforeach
								  </tbody>
                        </table>                        
                        <div class="form-group">
                        <input type="submit" id="submitted"  class="btn btn-success" value="Update">
                        </div>

                </form>
							</div>
                  </div>
                  @endsection