@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')

@include('error-messages.message')
<table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Size </th>
									  <th>Color</th>
									  <th>Price</th>
									</tr>
								  </thead>
								  <tbody>
                          <form action="{{route('admin.product.attributes.store')}}" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}	
                             @foreach($productAttributes as $productAttribute)
									<tr>
									  <td>{{$loop->iteration}}</td>
									  <input type="hidden" value="{{$productAttribute->product_id}}" name="product_id[]">
									  <td><input type="text" value="{{$productAttribute->size}}" name="size[]"></td>
									  <td><input type="text" value="{{$productAttribute->color}}" name="color[]"></td>
                    				  <td><input type="text" value="{{$productAttribute->price}}" name="price[]"></td>
                           </tr>
                           @endforeach
								  </tbody>
                        </table>                        
                        <div class="form-group">
                        <input type="submit" id="submitted"  class="btn btn-success" value="Update">
                        </div>

                </form>
							</div>
                  </div>
                  @endsection