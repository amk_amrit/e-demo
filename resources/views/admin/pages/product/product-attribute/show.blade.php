@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')
<div>
        <h5 style="text-align: center;">Product Attribute</h5>
	</div>
<table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Size</th>
									  <th>Color</th>
									  <th> Price(Rs)</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>
                                      @if($productAttributes->isEmpty())
                                      <tr >
                                          <td colspan="4">
                                          No Record Found
                                          </td>
                                      </tr>
                                      @endif
                                      @foreach($productAttributes as $productAttribute)
									<tr>
									  <td>{{$loop->iteration}}</td>
									  <td>{{ $productAttribute->size }}</td>
									  <td>{{ $productAttribute->color }}</td>
									  <td>Rs.{{ $productAttribute->price }}</td>
									  <td>
						{!! Form::open(['route' => ['admin.product.attributes.destroy', $productAttribute->id], 'method' => 'delete']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
									  </td>
                                    </tr>
                                    @endforeach
								  </tbody>
								</table>
							</div>
						</div>
                        <div>
                        @endsection