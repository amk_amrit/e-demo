@extends('admin.layouts.app')
@section('title','Product Attribute')
@section('content')

@include('error-messages.message')
<table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>Size </th>
									  <th>Color</th>
									  <th>Price</th>
									</tr>
								  </thead>
								  <tbody>
                          <form action="{{route('admin.product.attributes.store')}}" method="POST" enctype="multipart/form-data">
                          {{ csrf_field() }}	
									<tr>
									  <td><input type="text" value="{{old('size')}}" name="size[]"></td>
									  <td><input type="text" value="{{old('color')}}" name="color[]"></td>
                    				  <td><input type="text" value="{{old('price')}}" name="price[]"></td>
                           </tr>
								  </tbody>
                        </table>                        
                        <div class="form-group">
                        <input type="submit" id="submitted"  class="btn btn-success" value="Update">
                        </div>

                </form>
							</div>
                  </div>
                  @endsection