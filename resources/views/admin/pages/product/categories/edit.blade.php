@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Update</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.product.categories.update', $productCategories->id)}}" method="POST" enctype="multipart/form-data" >
                                            {{ csrf_field() }}	
                                            <input type="hidden" name="_method" value="PUT">
									
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Category Name</label>
											<div class="controls">
												<input type="text" name="product_category_name" id="basicinput" placeholder="Type Category Name here..." class="span8" value="{{$productCategories->product_category_name}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Status</label>
											<div class="controls">
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultChecked" name="status"  value="1" {{$productCategories->status == 1 ? 'checked' : '' }} >
													Active
												</label> 
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$productCategories->status == 0 ? 'checked' : '' }} >
													Deactive
												</label> 
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{$productCategories->description}}</textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Update  </button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection