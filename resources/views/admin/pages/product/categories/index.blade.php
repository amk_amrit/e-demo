@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<a href="{{route('admin.product.categories.create')}}" class="btn btn-info">Create New Product Category</a>
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Product Category list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>Category Name</th>
											<th>Description</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($productCategories as $productCategory)
										<tr class="odd gradeX">
											<td>{{ $productCategory->product_category_name }}</td>
											<td>{{$productCategory->description}}</td>
											<td>
												@if($productCategory->status==1)
												Active
												@else
												Deactivate
												@endif
											</td>
											<td>
												<a href="{{route('admin.product.categories.edit', $productCategory->id)}}" class="btn btn-info">Edit</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection