@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Update</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.products.update', $product->id)}}" method="POST" enctype="multipart/form-data" >
                                            {{ csrf_field() }}	
                                            <input type="hidden" name="_method" value="PUT">
											<div class="control-group">
											<label class="control-label" for="basicinput">Category</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="product_category_id">
													<option value="" disabled selected>Select here..</option>
													@foreach($productCategories as $productCategory)
													<option value="{{$productCategory->id}}" {{($product->product_category_id == $productCategory->id) ? 'selected' : ''}}  >{{$productCategory->product_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>			
										<div class="control-group">
											<label class="control-label" for="basicinput">Sub Category</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="product_sub_category_id">
													<option value="" disabled selected>Select here..</option>
													@foreach($productSubCategories as $productSubCategory)
													<option value="{{$productSubCategory->id}}" {{($product->product_sub_category_id == $productSubCategory->id) ? 'selected' : ''}} >{{$productSubCategory->product_sub_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>			
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Product Name</label>
											<div class="controls">
												<input type="text" name="product_name" id="basicinput" placeholder="Type Product Name here..." class="span8" value="{{$product->product_name}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Price($)</label>
											<div class="controls">
												<input type="text" name="main_price" id="basicinput" placeholder="Type  here..." class="span8" value="{{$product->main_price}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Thump Image</label>
											<div class="controls">
												<input type="file" name="thump_images" id="basicinput" placeholder="Type Product Name here..." class="span8" value="{{$product->thump_images}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Status</label>
											<div class="controls">
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultChecked" name="status"  value="1" {{$product->status == 1 ? 'checked' : '' }} >
													Active
												</label> 
												<label class="radio">
                                                <input type="radio" class="custom-control-input" id="defaultUnchecked" name="status" value="0" {{$product->status == 0 ? 'checked' : '' }} >
													Deactive
												</label> 
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{$product->description}}</textarea>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Update  </button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection