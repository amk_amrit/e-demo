@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<a href="{{route('admin.products.create')}}" class="btn btn-info">Create New Product </a>
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Product list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>Product Name</th>
											<th>Status</th>
											<th>Action</th>
											<th>Attribute Action</th>
											<th>Image Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($products as $product)
										<tr class="odd gradeX">
											<td>{{ $product->product_name }}</td>
											<td>
												@if($product->status==1)
												Active
												@else
												Deactivate
												@endif
											</td>
											<td>
												<a href="{{route('admin.products.show', $product->id)}}" class="btn btn-info">View</a>
												<a href="{{route('admin.products.edit', $product->id)}}" class="btn btn-primary">Edit</a>

											</td>
											<td>
												<a href="{{route('admin.product.attributes.show', $product->id)}}" class="btn btn-info">View</a>
												<a href="{{route('admin.product.attributes.edit', $product->id)}}" class="btn btn-primary">Create/Edit</a>

											</td>
											<td>
												<a href="{{route('admin.product.image.create', $product->id)}}" class="btn btn-primary">create</a>
												<a href="{{route('admin.product.images.show', $product->id)}}" class="btn btn-info">View</a>
												<a href="{{route('admin.product.images.edit', $product->id)}}" class="btn btn-primary">Edit</a>


											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection