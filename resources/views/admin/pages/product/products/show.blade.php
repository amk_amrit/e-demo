@extends('admin.layouts.app')

@section('content')
<div>
        <h5 style="text-align: center;">Product Details</h5>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" align="center" style="width: 70%;">
                        <tr>
                                <th>Category Name</th>
                                <td>{{$products->ProductCategories->product_category_name}}</td>
                            </tr>
                            <tr>
                                <th>Sub Category Name</th>
                                <td>{{$products->ProductSubCategory->product_sub_category_name}}</td>
                            </tr>
                            <tr>
                                <th>Product Name</th>
                                <td>{{$products->product_name}}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{$products->description}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($products->status==1)
                                    Active
                                    @else
                                    Deactive
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div>
        <h5 style="text-align: center;">Product Attribute</h5>
    </div>
    <table class="table table-striped table-bordered table-condensed">
								  <thead>
									<tr>
									  <th>#</th>
									  <th>Size</th>
									  <th>Color</th>
									  <th> Price(Rs)</th>
									</tr>
								  </thead>
								  <tbody>
                                      @if($productAttributes->isEmpty())
                                      <tr >
                                          <td colspan="4">
                                          No Record Found
                                          </td>
                                      </tr>
                                      @endif
                                      @foreach($productAttributes as $productAttribute)
									<tr>
									  <td>{{$loop->iteration}}</td>
									  <td>{{ $productAttribute->size }}</td>
									  <td>{{ $productAttribute->color }}</td>
									  <td>Rs.{{ $productAttribute->price }}</td>
                                    </tr>
                                    @endforeach
								  </tbody>
								</table>
							</div>
						</div>
                        <div>
        <h5 style="text-align: center;">Product Images</h5>
    </div> 
    
    <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-body">
                                @if($productImages->isEmpty())
                                No Record Found
                                @endif
                            @foreach($productImages as $productImage)

                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="media user">
                                        <?php $url=url('productImage/'.$productImage->images); 
                                				?>
                                               <a class="media-avatar pull-left" href="{{ asset('/productImage/' .$productImage->images)}}">
                                               <img class="d-block w-100" src="{{$url}}"  alt="Snow" height="100px" width="100px">
                                            </a>

                                            <div class="media-body">
                                                <table class="table" align="center" style="width: 70%;">
                                                        <tr>
                                                            <th>Size</th>
                                                            <td>{{$productImage->productImageAttribute->size}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Color</th>
                                                            <td>{{$productImage->productImageAttribute->color}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Price</th>
                                                            <td>Rs.{{$productImage->productImageAttribute->price}}</td>
                                                        </tr>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="media user">
                                            <a class="media-avatar pull-left" href="#">
                                            <img src="{{asset('admin/images/user.png')}}">
                                            </a>
                                            <div class="media-body">
                                                <table class="table" align="center" style="width: 70%;">
                                                        <tr>
                                                            <th>Size</th>
                                                            <td>{{$productImage->productImageAttribute->size}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Color</th>
                                                            <td>{{$productImage->productImageAttribute->color}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Price</th>
                                                            <td>Rs.{{$productImage->productImageAttribute->price}}</td>
                                                        </tr>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/.row-fluid-->
                                <br />
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--/.content-->
                </div>
                <!--/.span9-->

       
@endsection