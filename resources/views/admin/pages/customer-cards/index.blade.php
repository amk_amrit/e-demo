@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<!-- <a href="" class="btn btn-info">Create New Customer</a> -->
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Customer Card List</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>User Name</th>
											<th>Card Holder Name</th>
											<th>Expire Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($customrecards as $customrecard)
										<tr class="odd gradeX">
											<td>{{$customrecard->users->name }}({{ $customrecard->users->email }})</td>
											<td>{{$customrecard->card_holder_name}}</td>
											<td>{{$customrecard->expire_date}}</td>
											<td>
												<a href="{{route('admin.customer-cards.show', $customrecard->id)}}" class="btn btn-info">view Details</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection