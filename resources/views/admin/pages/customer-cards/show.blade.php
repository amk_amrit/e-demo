@extends('admin.layouts.app')

@section('content')
<div>
        <h5 style="text-align: center;">User Details</h5>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" align="center" style="width: 70%;">
                        <tr>
                                <th> User Name</th>
                                <td>{{$customerCards->users->name}}({{ $customerCards->users->email }})</td>
                            </tr>
                            <tr>
                                <th>Card Holder Name</th>
                                <td>{{$customerCards->card_holder_name}}</td>
                            </tr>
                            <tr>
                                <th>Card Number</th>
                                <td>{{$customerCards->card_number}}</td>
                            </tr>
                            <tr>
                                <th>Expire Date</th>
                                <td>{{$customerCards->expire_date}}</td>
                            </tr>
                            <tr>
                                <th>CVC Number </th>
                                <td>{{$customerCards->cvc_number}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div>
	@endsection