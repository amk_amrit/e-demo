@extends('admin.layouts.app')

@section('content')
<div>
        <h5 style="text-align: center;">User Details</h5>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" align="center" style="width: 70%;">
                        <tr>
                                <th> Name</th>
                                <td>{{$customers->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$customers->email}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{$customers->address}}</td>
                            </tr>
                            <tr>
                                <th>Facebook Id</th>
                                <td>{{$customers->facebook_id}}</td>
                            </tr>
                            <tr>
                                <th>Google Id</th>
                                <td>{{$customers->google_id}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($customers->status==1)
                                    Active
                                    @else
                                    Deactive
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Pan Number</th>
                                <td>{{$customers->pan_number}}</td>
                            </tr>
                            <tr>
                                <th>Phone Number</th>
                                <td>{{$customers->phone}}</td>
                            </tr>
                            <tr>
                                <th>Mobile Number</th>
                                <td>{{$customers->mobile_number}}</td>
                            </tr>
                            <tr>
                                <th>Customer type</th>
                                <td>{{$customers->user_types }}</td>
                            </tr>
                            <tr>
                                <th>About Us</th>
                                <td>{{$customers->about_us}}</td>
                            </tr>
                            <tr>
                                <th>Messanger Id</th>
                                <td>{{$customers->messanger_id}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div>
	@endsection