@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<!-- <a href="" class="btn btn-info">Create New Customer</a> -->
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Customer list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th> Name</th>
											<th>Email</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($customers as $customer)
										<tr class="odd gradeX">
											<td>{{ $customer->name }}</td>
											<td>{{$customer->email}}</td>
											<td>
												@if($customer->status==1)
												Active
												@else
												Deactivate
												@endif
											</td>
											<td>
												<a href="{{route('admin.customers.show', $customer->id)}}" class="btn btn-info">view Details</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection