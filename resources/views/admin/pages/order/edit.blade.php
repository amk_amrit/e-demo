@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">

						<div class="module">
							<div class="module-head">
								<h3>Update</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.orders.update', $order_list->id)}}" method="POST" enctype="multipart/form-data" >
                                            {{ csrf_field() }}	
                                            <input type="hidden" name="_method" value="PUT">

											<div class="control-group">
											<label class="control-label" for="basicinput">Status</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="status">
													<option value="">Select here..</option>
                                                    <option value="approved" >Approved</option>
                                                    <option value="panding" >Panding</option>
                                                    <option value="complet" >Complet</option>

												</select>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Update </button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection