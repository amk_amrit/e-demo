@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')

						<div class="module">
							<div class="module-head">
								<h3>Order list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>User Id</th>
											<th>Product Name</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($orders as $order)
										<tr class="odd gradeX">
											<td>{{ $order->order_user_id }}</td>
											<td>{{$order->orderProduct->product_name}}</td>
											<td>
												@if($order->status=="panding")
												Panding
												@elseif($order->status=="approved")
												Approved
												@else
												Complet
												@endif
											</td>
											<td>
												<a href="{{route('admin.orders.edit', $order->id)}}" class="btn btn-info">View Payment</a>
												<a href="{{route('admin.orders.edit', $order->id)}}" class="btn btn-info">Change The Status</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection