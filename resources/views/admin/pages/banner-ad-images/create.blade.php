@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">
						<div class="module">
							<div class="module-head">
								<h3>Create</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.banner-ad-images.store')}}" method="POST" enctype="multipart/form-data" >
                                            {{ csrf_field() }}										
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Banner Name</label>
											<div class="controls">
												<input type="text" name="banner_add_name" id="basicinput" placeholder="Type Banner Name here..." class="span8" value="{{old('banner_add_name')}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Starting From($)</label>
											<div class="controls">
												<input type="number" name="price" id="basicinput" placeholder="Type here..." class="span8" value="{{old('price')}}">
											</div>
										</div>
													
										<div class="control-group">
											<label class="control-label" for="basicinput">Image</label>
											<div class="controls">
												<input type="file" name="images" id="basicinput" class="span8" value="{{old('images')}}" required>
											</div>
										</div>	

										<div class="control-group">
											<label class="control-label" for="basicinput">Image Type</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="image_types">
													<option value="" disabled selected>Select here..</option>
													<option value="banner_ad_image">Banner Ad</option>
													<option value="slider_image">Slider</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Sub Category Link Id</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="sub_category_link_id">
													<option value="" disabled selected>Select here..</option>
													@foreach($subCategoryLists as $subCategoryList)
													<option value="{{$subCategoryList->id}}" {{ (old ('sub_category_link_id')==$subCategoryList->id)? "selected" : ''}}>{{$subCategoryList->product_sub_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Order</label>
											<div class="controls">
												<input type="number" name="order" id="basicinput" placeholder="Type Image oder here..." class="span8" value="{{old('order')}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{Request::old('description')}}  </textarea>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Create</button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection