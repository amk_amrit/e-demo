@extends('admin.layouts.app')

@section('content')
				<div class="span9">
					<div class="content">
						<div class="module">
							<div class="module-head">
								<h3>Edit</h3>
							</div>
							<div class="module-body">
							@include('error-messages.message')
									<form class="form-horizontal row-fluid" action="{{route('admin.banner-ad-images.update', $bannerAdImages->id)}}" method="POST" enctype="multipart/form-data" >
											{{ csrf_field() }}	
											<input type="hidden" name="_method" value="PUT">
									
                                            <div class="control-group">
											<label class="control-label" for="basicinput">Banner Name</label>
											<div class="controls">
												<input type="text" name="banner_add_name" id="basicinput" placeholder="Type Banner Name here..." class="span8" value="{{$bannerAdImages->banner_add_name}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Starting From($)</label>
											<div class="controls">
												<input type="number" name="price" id="basicinput" placeholder="Type  here..." class="span8" value="{{$bannerAdImages->price}}">
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Order</label>
											<div class="controls">
												<input type="number" name="order" id="basicinput" placeholder="Type Image oder here..." class="span8" value="{{$bannerAdImages->order}}">
											</div>
										</div>													
										<div class="control-group">
											<label class="control-label" for="basicinput">Image</label>
											<div class="controls">
											<?php $url=url('BannerImage/'.$bannerAdImages->images); 
                                				?>
												<input type="file" name="images" id="basicinput" class="span8" value="{{ $bannerAdImages->images }}">
												<a href="{{ asset('/BannerImage/' .$bannerAdImages->images)}}"><img class="d-block w-100" src="{{$url}}"  alt="Snow" height="100px" width="100px"></a>

											</div>
										</div>	

										<div class="control-group">
											<label class="control-label" for="basicinput">Image Type</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="image_types">
													<option value="" disabled selected>Select here..</option>
													<option value="banner_ad_image" {{$bannerAdImages->image_types == 'banner_ad_image' ? 'selected' : '' }} >Banner Ad</option>
													<option value="slider_image" {{$bannerAdImages->image_types == 'slider_image' ? 'selected' : '' }}>Slider</option>
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Sub Category Link Id</label>
											<div class="controls">
												<select tabindex="1" data-placeholder="Select here.." class="span8" name="sub_category_link_id">
													<option value="" disabled selected>Select here..</option>
													@foreach($subCategoryLists as $subCategoryList)
													<option value="{{$subCategoryList->id}}" {{ $subCategoryList->id=$bannerAdImages->sub_category_link_id? "selected" : ''}}>{{$subCategoryList->product_sub_category_name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" for="basicinput">Description</label>
											<div class="controls">
												<textarea class="span8" rows="5" name="description">{{$bannerAdImages->description}}</textarea>
											</div>
										</div>
										<div class="control-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Update</button>
											</div>
										</div>
									</form>
							</div>
						</div>	
					</div><!--/.content-->
                </div><!--/.span9-->
        @endsection