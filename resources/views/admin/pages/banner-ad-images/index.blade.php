@extends('admin.layouts.app')
@section('title','Product Category')
@section('content')


					<div class="row">
						<div class="col-lg-12" style="text-align: right;">
					<a href="{{route('admin.banner-ad-images.create')}}" class="btn btn-info">Create</a>
						</div>
					</div>
						<div class="module">
							<div class="module-head">
								<h3>Banner Add Images list</h3>
							</div>
							<div class="module-body table">
							@include('error-messages.message')

								<table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
									<thead>
										<tr>
											<th>Image Name</th>
											<th>Order</th>
											<th>Image Type</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($bannerAdImages as $bannerAdImage)
										<tr class="odd gradeX">
											<td>{{ $bannerAdImage->banner_add_name }}</td>
											<td>{{$bannerAdImage->order}}</td>
											<td>{{ $bannerAdImage->image_types }}</td>
											<td>
												<a href="{{route('admin.banner-ad-images.edit', $bannerAdImage->id)}}" class="btn btn-info">Edit</a>
											</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div><!--/.module-->

					<br />
@endsection