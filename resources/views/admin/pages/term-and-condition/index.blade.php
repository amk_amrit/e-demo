@extends('admin.layouts.app')

@section('content')
@include('error-messages.message')
<form class="form-horizontal row-fluid" action="{{route('admin.term-and-conditions.update',$termAndCondition->id )}}" method="POST" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">	        
        <div class="control-group">
                <label class="control-label" for="basicinput">Term And Condition</label>
	                <div class="controls">
		                <textarea class="span8" rows="5" name="term_and_condition">{{ $termAndCondition->term_and_condition }}</textarea>
		        </div>
        </div> 
        <div class="control-group">
	    <div class="controls">
	        <button type="submit" class="btn btn-primary">Update</button>
	    </div>
	</div>
</form>
@endsection