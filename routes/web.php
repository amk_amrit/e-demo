<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\FrontendController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Admin Url 
Route::get('/admin/login', 'Admin\AdminLoginController@index')->name('admin');
Route::post('/admin/dastboard', 'Admin\AdminLoginController@login')->name('admin');
Route::post('/admin/logout', 'Admin\AdminLoginController@logout')->name('admin.logout');
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function(){
    Route::group(['prefix' =>'product', 'as' => 'product.' ], function(){
        Route::resource('/categories','Admin\Product\CategoriesController');
        Route::resource('/sub-categories','Admin\Product\SubCategoriesController');
        Route::resource('/attributes','Admin\Product\ProductAttributeController');
        //In product image controller update and store function are exchnage, 
        //update function work as store new data and store function work as update data
        Route::resource('/images','Admin\Product\ProductImageController');
        Route::get('/image/create/{id}','Admin\Product\ProductImageController@productImageCreate')->name('image.create');


    });
    Route::resource('/products','Admin\Product\ProductController');
    Route::resource('/term-and-conditions','Admin\TermAndConditionController');
    Route::resource('/orders','Admin\OrderController');
    Route::resource('/banner-ad-images','Admin\BannerAdImageController');
    Route::resource('/customers','Admin\CustomerController');
    Route::resource('/customer-cards','Admin\CustomerCardsController');
});
//Frontend Url
Route::get('categories\{id}', 'Frontend\FrontendController@getCategoryData')->name('categories');
Route::get('sub-categories\{id}', 'Frontend\FrontendController@getsubCategoryData')->name('sub-categories');
Route::get('products\{id}', 'Frontend\FrontendController@getSingleProduct')->name('products');
Route::get('about-us', 'Frontend\FrontendController@aboutUs')->name('about-us');
Route::get('contact-us', 'Frontend\FrontendController@contactUs')->name('contact-us');
Route::get('blogs', 'Frontend\FrontendController@getBlog')->name('blogs');


//Customer Login/Sign Up URL
Route::get('customer/login', 'Frontend\CustomerController@getLoginForm')->name('customer.login');
Route::post('customer/dastboard', 'Frontend\CustomerController@login')->name('customer.dastboard');
Route::get('customer/sign-up', 'Frontend\CustomerController@getSignUpForm')->name('customer.sign-up');
Route::post('customer/sign-up', 'Frontend\CustomerController@SignUp')->name('customer.sign-up');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

