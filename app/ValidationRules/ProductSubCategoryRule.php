<?php


namespace App\ValidationRules;

class ProductSubCategoryRule extends BaseRule
{
    public function getProductSubCategoryRules($ruleKey, $id=0){

        $rules =[
            'create' => [
                'product_sub_category_name' =>'required | unique:product_sub_categories',
                'product_category_id' => 'required'
              
            ],
            'update' => [
                'product_sub_category_name' => ['required', 'unique:product_sub_categories,product_sub_category_name,'.$id],
                'product_category_id' => 'required'
            ],
        ];

        return $rules[$ruleKey];
    }


}
