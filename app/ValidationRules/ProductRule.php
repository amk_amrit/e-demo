<?php


namespace App\ValidationRules;

class ProductRule extends BaseRule
{
    public function getProductRules($ruleKey, $id=0){

        $rules =[
            'create' => [
                'product_category_id' => 'required',
                'product_sub_category_id' => 'required',
                'product_name' =>'required | unique:products',
                'main_price' => 'required',
              
            ],
            'update' => [
                'product_category_id' => 'required',
                'product_sub_category_id' => 'required',
                'product_name' => ['required', 'unique:products,product_name,'.$id],
                'main_price' => 'required',
            ],
        ];

        return $rules[$ruleKey];
    }


}
