<?php


namespace App\ValidationRules;

class ProductAttributeRule extends BaseRule
{
    public function getProductAttributeRules($ruleKey, $id=0){

$rules =[
            'create' => [
                'size.*' => 'required',
                'color.*' => 'required',
                'price.*' =>'required'
              
            ],
            'update' => [
                'size.*' => 'required',
                'color.*' => 'required',
                'price.*' =>'required'
            ],
        ];
        return $rules[$ruleKey];
    }


}