<?php


namespace App\ValidationRules;

class ProductImageRule extends BaseRule
{
    public function getProductImageRules($ruleKey, $id=0){

$rules =[
            'create' => [
                'product_attribute_id.*' => 'required',
                'images.*' => 'required|regex:/^data:image/'
              
            ],
            'update' => [
                'product_attribute_id.*' => 'required',
                'images.*' => 'required|image'
            ],
        ];
        return $rules[$ruleKey];
    }


}