<?php


namespace App\ValidationRules;

class ProductCategoryRule extends BaseRule
{
    public function getProductCategoryRules($ruleKey, $id=0){

        $rules =[
            'create' => [
                'product_category_name' =>'required | unique:product_categories',
              
            ],
            'update' => [
                'product_category_name' => ['required', 'unique:product_categories,product_category_name,'.$id],
            ],
        ];

        return $rules[$ruleKey];
    }


}
