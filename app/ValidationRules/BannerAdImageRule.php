<?php


namespace App\ValidationRules;

class BannerAdImageRule extends BaseRule
{
    public function getBannerAdImageRules($ruleKey, $id=0){

$rules =[
            'create' => [
                'banner_add_name' => 'required',
                // 'images' => 'required',
                'order' =>' unique:banner_ad_images',
                'image_types' => 'required'
              
            ],
            'update' => [
                'banner_add_name' => 'required',
                // 'images' => 'required',
                'order' => 'unique:banner_ad_images,order,'.$id ,
                'image_types' => 'required'
            ],
        ];
        return $rules[$ruleKey];
    }


}