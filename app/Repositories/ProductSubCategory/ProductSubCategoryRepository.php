<?php 
namespace App\Repositories\ProductSubCategory;

use App\Models\ProductSubCategory;
use App\Models\ProductCategory;
use DB;
use Exception;

class ProductSubCategoryRepository implements ProductSubCategoryInterface
{
  
    public function all(){
        return $productSubCategory=ProductSubCategory::with('productCategory')->get();
    }
    public function allCategory(){
        return $productSubCategory=ProductCategory::where('status', '1')->get();
    }
    public function store($data){
        $productSubCategory=new ProductSubCategory;

        $productSubCategory->product_category_id=$data['product_category_id'];
        $productSubCategory->product_sub_category_name=$data['product_sub_category_name'];
        $productSubCategory->description=$data['description'];
        $productSubCategory->status=$data['status'];
        $productSubCategory->created_by=1;
        $productSubCategory->updated_by=1;
        $productSubCategory->save();
    }
    public function findById($id){
        return $productCategory=ProductSubCategory::with('productCategory')->find($id);
    }
    public function update($data, $id){
        $productSubCategory=ProductSubCategory::find($id);

        $productSubCategory->product_category_id=$data['product_category_id'];
        $productSubCategory->product_sub_category_name=$data['product_sub_category_name'];
        $productSubCategory->description=$data['description'];
        $productSubCategory->status=$data['status'];
        $productSubCategory->created_by=1;
        $productSubCategory->updated_by=1;
        $productSubCategory->save();
    }
}