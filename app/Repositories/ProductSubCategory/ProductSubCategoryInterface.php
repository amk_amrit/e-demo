<?php
 namespace App\Repositories\ProductSubCategory;

interface ProductSubCategoryInterface
{
 
    public function all();
    public function allCategory();
    public function store($data);
    // public function findById($id);
    // public function update($data, $id);
}


