<?php 
namespace App\Repositories\TermAndCondition;

use App\Models\TermAndCondition;
use DB;
use Exception;

class TermAndConditionRepository implements TermAndConditionInterface
{
  
    public function all(){
        return $termAndCondition=TermAndCondition::first();
    }
//     public function store($data){
//         $Product=new Product;

//         $Product->product_category_id=$data['product_category_id'];
//         $Product->product_sub_category_id=$data['product_sub_category_id'];
//         $Product->product_name=$data['product_name'];
//         $Product->description=$data['description'];
//         $Product->status=$data['status'];
//         $Product->created_by=1;
//         $Product->updated_by=1;
//         $Product->save();
//     }
//     public function findById($id){
//          return $product=Product::with('ProductCategories','ProductSubCategory')->find($id);
//     }
//     public function productAttribute($id){
//         return $productAttribute=ProductAttribute::with('productAttribute')->where('product_id', $id)->get();
//    }
//    public function productImage($id){
//     return $productImage=ProductImage::with('productImage','productImageAttribute')->where('product_id', $id)->get();
// }
// public function productCategory()
// {
//     return $productCategory=ProductCategory::where('status', 1)->get();
// }
// public function productSubCategory()
// {
//     return $productSubCategory=ProductSubCategory::where('status', 1)->get();
// }
    public function update($data, $id){
        $Product=TermAndCondition::find($id);
        $Product->term_and_condition=$data['term_and_condition'];
        $Product->created_by=1;
        $Product->updated_by=1;
        $Product->save();
    }
//     public function productAttributeList($id)
//     {
//        return $productAttributeList=ProductAttribute::where('product_id', $id)->get();
//     }
//     public function productAttributeStore($data){
//         dd($data);
//     }
}