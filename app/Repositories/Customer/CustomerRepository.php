<?php 
namespace App\Repositories\Customer;

use App\Models\UserPaymentCardInformations;
use App\User;
use DB;
use Exception;

class CustomerRepository implements CustomerInterface
{
  
    public function all(){
        return $customer=User::all();
    }

    public function findById($id){
        return $customres=User::find($id);
    }
    public function customerCard(){
       return $customerCard=UserPaymentCardInformations::with('users')->get();
    }
    public function findByIdCustomerCard($id){
        return $customerCard=UserPaymentCardInformations::with('users')->find($id);
    }

}