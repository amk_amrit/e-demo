<?php
 namespace App\Repositories\Customer;

interface CustomerInterface
{
 
    public function all();
    public function findById($id);
    public function customerCard();
    public function findByIdCustomerCard($id);
}


