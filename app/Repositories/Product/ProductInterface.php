<?php
 namespace App\Repositories\Product;

interface ProductInterface
{
 
    public function all();
    public function store($data);
    public function findById($id);
    public function productAttribute($id);
    public function productImage($id);
    public function productCategory();
    public function productSubCategory();
    public function update($data, $id);
    public function productAttributeList($id);
    public function productAttributeStore($data);
    public function productAttributeDelete($id);
    public function productImageList($id);
    public function productImageUpdate($data);
    public function productImageDelete($id);
    public function productImageStore($data, $id);

}


