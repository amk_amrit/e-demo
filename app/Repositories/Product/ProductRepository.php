<?php 
namespace App\Repositories\Product;

use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductSubCategory;
use DB;
use Exception;
use Image;
use File;

class ProductRepository implements ProductInterface
{
  
    public function all(){
        return $product=Product::all();
    }
    public function store($data){
        $Product=new Product;

        $Product->product_category_id=$data['product_category_id'];
        $Product->product_sub_category_id=$data['product_sub_category_id'];
        $Product->product_name=$data['product_name'];
        $Product->description=$data['description'];
        $Product->status=$data['status'];
        $Product->main_price=$data['main_price'];
        $Product->created_by=1;
        $Product->updated_by=1;
        if (isset($data['thump_images'])) {
            $file=$data['thump_images'];
            $file_name=microtime().'.'. $file->getClientOriginalExtension();
            $location=('productImage/' .$file_name);
            Image::make($file)->resize(600, 'auto')->save($location);
            $Product->thump_images=$file_name;
        }
        $Product->save();
    }
    public function findById($id){
         return $product=Product::with('ProductCategories','ProductSubCategory')->find($id);
    }
    public function productAttribute($id){
        return $productAttribute=ProductAttribute::with('productAttribute')->where('product_id', $id)->get();
   }
   public function productImage($id){
    return $productImage=ProductImage::with('productImage','productImageAttribute')->where('product_id', $id)->get();
}
public function productCategory()
{
    return $productCategory=ProductCategory::where('status', 1)->get();
}
public function productSubCategory()
{
    return $productSubCategory=ProductSubCategory::where('status', 1)->get();
}
    public function update($data, $id){
        $Product=Product::find($id);
        $Product->product_category_id=$data['product_category_id'];
        $Product->product_sub_category_id=$data['product_sub_category_id'];
        $Product->product_name=$data['product_name'];
        $Product->description=$data['description'];
        $Product->status=$data['status'];
        $Product->main_price=$data['main_price'];
        $Product->created_by=1;
        $Product->updated_by=1;
        if (isset($data['thump_images'])) {
            $file=$data['thump_images'];
            $file_name=microtime().'.'. $file->getClientOriginalExtension();
            $location=('productImage/' .$file_name);
            Image::make($file)->resize(600, 'auto')->save($location);
            $deleteImages=public_path("productImage/{$Product->thump_images}");
            if(File::exists($deleteImages)){
                unlink($deleteImages);
            }
            $Product->thump_images=$file_name;
        }
        $Product->save();
    }
    public function productAttributeList($id)
    {
       return $productAttributeList=ProductAttribute::where('product_id', $id)->get();
    }
    public function productAttributeStore($data){
        foreach ($data['size'] as $key => $size) {

            ProductAttribute::updateOrCreate(
                [
                    'product_id' => $data['product_id'][$key],
                    
                ],
                [
                    'size' => $data['size'][$key],
                    'color' => $data['color'][$key],
                    'price' => $data['price'][$key],
                    'created_by' =>1,
                    'updated_by' => 1,

                ]
            );
        }    }
    public function productAttributeDelete($id){
        $productAttributeId=ProductAttribute::find($id);
        $productAttributeId->delete();
    }
    public function productImageList($id)
    {
        return $productImageList=ProductImage::where('product_id', $id)->get();
    }
    public function productImageUpdate($data)
    {
        //dd($data);
    //     foreach ($data['product_id'] as $key => $size) {

    //         if ($data['productImage'][$key]) {

    //             $file=$data['productImage'][$key];
    //             $file_name=time().'.'. $file->getClientOriginalExtension();
    //             $location=('productImage/' .$file_name);
    //             Image::make($file)->resize(600, 'auto')->save($location);
    //         }

    //         ProductImage::updateOrCreate(
    //             [
    //                 'product_id' => $data['product_id'][$key],
                    
    //             ],
    //             [
    //                 'product_attribute_id' => $data['product_attribute_id'][$key],
    //                 'images' => $file_name,
    //                 'created_by' =>1,
    //                 'updated_by' => 1,

    //             ]
    //         );
    //     }
    
        foreach ($data['product_id'] as $key => $size) {

            $productImage=ProductImage::find($data['id'][$key]);
            $productImage->product_id=$data['product_id'][$key];
            $productImage->product_attribute_id=$data['product_attribute_id'][$key];
            $productImage->created_by=1;
            $productImage->updated_by=1;
            if (isset($data['productImage'][$key])) {
                            $file=$data['productImage'][$key];
                            $file_name=microtime().'.'. $file->getClientOriginalExtension();
                            $location=('productImage/' .$file_name);
                            Image::make($file)->resize(600, 'auto')->save($location);
                            $deleteImages=public_path("productImage/{$productImage->images}");
                            if(File::exists($deleteImages)){
                                unlink($deleteImages);
                            }
                            $productImage->images=$file_name;
                        }
            $productImage->save();
        }


         }
         public function productImageDelete($id){
            $deleteImages=ProductImage::find($id);
            $deleteImages->delete();
            $deleteImages=public_path("productImage/{$deleteImages->images}");
            if(File::exists($deleteImages)){
                unlink($deleteImages);
            }  
        }
        public function productImageStore($data, $id)
        {
            foreach ($data['product_attribute_id'] as $key => $product_attribute_id) {

                $productImage= new ProductImage;
                $productImage->product_id=$id;
                $productImage->product_attribute_id=$data['product_attribute_id'][$key];
                $productImage->created_by=1;
                $productImage->updated_by=1;
                if (isset($data['productImage'][$key])) {
                                $file=$data['productImage'][$key];
                                $file_name=microtime().'.'. $file->getClientOriginalExtension();
                                $location=('productImage/' .$file_name);
                                Image::make($file)->resize(600, 'auto')->save($location);
                                $productImage->images=$file_name;
                            }
                $productImage->save();
            }
    
    
            
        }
}