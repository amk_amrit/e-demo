<?php
 namespace App\Repositories\BannerAdImage;

interface BannerAdImageInterface
{
 
    public function all();
    public function store($data);
    public function update($data, $id);
    public function findById($id);
    public function subCategoryList();

}


