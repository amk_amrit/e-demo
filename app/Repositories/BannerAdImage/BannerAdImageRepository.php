<?php 
namespace App\Repositories\BannerAdImage;

use App\Models\BannerAdImage;
use App\Models\ProductSubCategory;
use DB;
use Exception;
use Image;
use File;


class BannerAdImageRepository implements BannerAdImageInterface
{
  
    public function all(){
        return $bannerAdImages=BannerAdImage::get();
    }
    public function store($data){
        $bannerAdImage=new BannerAdImage;

        $bannerAdImage->banner_add_name=$data['banner_add_name'];
        $bannerAdImage->order=$data['order'];
        $bannerAdImage->image_types=$data['image_types'];   
        $bannerAdImage->price=$data['price'];   
        $bannerAdImage->description=$data['description'];   
        $bannerAdImage->sub_category_link_id=$data['sub_category_link_id'];   

        $bannerAdImage->created_by=1;
        $bannerAdImage->updated_by=1;
        if (isset($data['images'])) {
            $file=$data['images'];
            $file_name=microtime().'.'. $file->getClientOriginalExtension();
            $location=('BannerImage/' .$file_name);
            Image::make($file)->resize('600', 'auto')->save($location);
            $bannerAdImage->images=$file_name;
        }
        $bannerAdImage->save();
    }
    public function update($data, $id){
        $bannerAdImage=BannerAdImage::find($id);
        $bannerAdImage->banner_add_name=$data['banner_add_name'];
        $bannerAdImage->order=$data['order'];
        $bannerAdImage->image_types=$data['image_types'];
        $bannerAdImage->price=$data['price'];   
        $bannerAdImage->description=$data['description'];   
        $bannerAdImage->sub_category_link_id=$data['sub_category_link_id'];        
        $bannerAdImage->created_by=1;
        $bannerAdImage->updated_by=1;

        if (isset($data['images'])) {
            $file=$data['images'];
            $file_name=microtime().'.'. $file->getClientOriginalExtension();
            $location=('BannerImage/' .$file_name);
            Image::make($file)->resize(600, 'auto')->save($location);
            $deleteImages=public_path("productImage/{$bannerAdImage->images}");
            if(File::exists($deleteImages)){
                unlink($deleteImages);
            }
            $bannerAdImage->images=$file_name;
        }
        $bannerAdImage->save();
    }
    public function findById($id){
        return $bannerAdImage=BannerAdImage::find($id);
    }
    public function subCategoryList(){
        return $subCategoryList=ProductSubCategory::where('status',1)->get();
    }

}