<?php 
namespace App\Repositories\ProductCategory;

use App\Models\ProductCategory;
use DB;
use Exception;

class ProductCategoryRepository implements ProductCategoryInterface
{
  
    public function all(){
        return $productCategory=ProductCategory::all();
    }
    public function store($data){
        $productCategory=new ProductCategory;

        $productCategory->product_category_name=$data['product_category_name'];
        $productCategory->description=$data['description'];
        $productCategory->status=$data['status'];
        $productCategory->created_by=1;
        $productCategory->updated_by=1;
        $productCategory->save();
    }
    public function findById($id){
        return $productCategory=ProductCategory::find($id);
    }
    public function update($data, $id){
        $productCategory=ProductCategory::find($id);
        $productCategory->product_category_name=$data['product_category_name'];
        $productCategory->description=$data['description'];
        $productCategory->status=$data['status'];
        $productCategory->created_by=1;
        $productCategory->updated_by=1;
        $productCategory->save();
    }
}