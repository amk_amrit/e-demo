<?php
 namespace App\Repositories\ProductCategory;

interface ProductCategoryInterface
{
 
    public function all();
    public function store($data);
    public function findById($id);
    public function update($data, $id);
}


