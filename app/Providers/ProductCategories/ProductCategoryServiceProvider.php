<?php

namespace App\Providers\ProductCategories;

use App\Repositories\ProductCategory\ProductCategoryInterface;
use App\Repositories\ProductCategory\ProductCategoryRepository;
use Illuminate\Support\ServiceProvider;

class ProductCategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(ProductCategoryInterface::class, ProductCategoryRepository::class);
    }
}
