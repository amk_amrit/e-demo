<?php

namespace App\Providers\Customer;

use App\Repositories\Customer\CustomerInterface;
use App\Repositories\Customer\CustomerRepository;
use Illuminate\Support\ServiceProvider;

class CustomerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
    }
}
