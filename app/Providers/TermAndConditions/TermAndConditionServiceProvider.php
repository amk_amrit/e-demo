<?php

namespace App\Providers\TermAndConditions;

use App\Repositories\TermAndCondition\TermAndConditionRepository;
use App\Repositories\TermAndCondition\TermAndConditionInterface;
use Illuminate\Support\ServiceProvider;

class TermAndConditionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(TermAndConditionInterface::class, TermAndConditionRepository::class);
    }
}
