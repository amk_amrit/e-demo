<?php

namespace App\Providers\ProductSubCategories;

use App\Repositories\ProductSubCategory\ProductSubCategoryInterface;
use App\Repositories\ProductSubCategory\ProductSubCategoryRepository;
use Illuminate\Support\ServiceProvider;

class ProductSubCategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(ProductSubCategoryInterface::class, ProductSubCategoryRepository::class);
    }
}
