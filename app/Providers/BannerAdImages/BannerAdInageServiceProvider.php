<?php

namespace App\Providers\BannerAdImages;

use App\Repositories\BannerAdImage\BannerAdImageInterface;
use App\Repositories\BannerAdImage\BannerAdImageRepository;
use Illuminate\Support\ServiceProvider;

class BannerAdInageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(BannerAdImageInterface::class, BannerAdImageRepository::class);
    }
}
