<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    protected $fillable=['product_id','product_attribute_id','images','created_by','updated_by'];

    public function productImage(){
        return $this->belongsTo(Product::class, 'product_id')->withDefault();
    }
    public function productImageAttribute(){
        return $this->belongsTo(ProductAttribute::class, 'product_attribute_id')->withDefault();
    }
}
