<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    //
    protected $fillable=['product_category_name','description','status','created_by','updated_by'];

    public function productCategory(){
        return $this->hasMany(ProductSubCategory::class, 'product_category_id');
    }
    public function ProductCategories(){
        return $this->hasMany(Product::class, 'product_category_id');
    }

}
