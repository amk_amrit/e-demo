<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function orderProduct(){
        return $this->belongsTo(Product::class, 'product_name');
    }
}
