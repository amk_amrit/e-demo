<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    //
    protected $fillable=['product_sub_category_name','product_category_id','description','status','created_by','updated_by'];

    public function productCategory(){
        return $this->belongsTo(ProductCategory::class, 'product_category_id')->withDefault();
    }

    public function ProductSubCategory(){
        return $this->hasMany(Product::class, 'product_sub_category_id');
    }
}
