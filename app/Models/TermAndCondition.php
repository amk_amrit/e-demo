<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermAndCondition extends Model
{
    //
    protected $fillable=['term_and_condition','created_by','updated_by'];
}
