<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    //
    protected $fillable=['product_id','size','color','price','created_by','updated_by'];

    public function productAttribute(){
        return $this->belongsTo(ProductAttribute::class, 'product_id');
    }
    public function productImageAttribute(){
        return $this->hasMany(ProductImage::class, 'product_attribute_id');
    }
}
