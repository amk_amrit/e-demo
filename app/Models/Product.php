<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $fillable=['product_category_id','product_sub_category_id','product_name','description ','status','created_by','updated_by'];

    public function ProductCategories(){
        return $this->belongsTo(ProductCategory::class, 'product_category_id')->withDefault();
    }
    public function ProductSubCategory(){
        return $this->belongsTo(ProductSubCategory::class, 'product_sub_category_id')->withDefault();
    }
    public function productAttribute(){
        return $this->hasMany(ProductAttribute::class, 'product_id');
    }
    public function productImage(){
        return $this->hasMany(ProductImage::class, 'product_id');
    }
    public function orderProduct(){
        return $this->hasMany(Order::class, 'product_name');
    }
}
