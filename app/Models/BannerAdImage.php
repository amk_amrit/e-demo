<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerAdImage extends Model
{
    protected $fillable=['banner_add_name','images','order','image_types','created_by','updated_by'];
    //
}
