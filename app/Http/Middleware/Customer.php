<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $section_for_admin=Session::get('customer_name');
        if (empty($section_for_admin)) {

            return redirect('/customer/login');   
        }   
            return $next($request);
    }
}
