<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BannerAdImage\BannerAdImageInterface;
use App\ValidationRules\BannerAdImageRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BannerAdImageController extends Controller
{
    private $BannerAdImageReopsitories;

    public function __construct(BannerAdImageInterface $BannerAdImageReopsitories){
            $this->BannerAdImageReopsitories=$BannerAdImageReopsitories;
            $this->middleware('Admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bannerAdImage=$this->BannerAdImageReopsitories->all();
        return \view('admin.pages.banner-ad-images.index')->with('bannerAdImages', $bannerAdImage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subCategoryList=$this->BannerAdImageReopsitories->subCategoryList();
        return \view('admin.pages.banner-ad-images.create')->with('subCategoryLists', $subCategoryList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(),(new BannerAdImageRule($request))->getBannerAdImageRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $bannerAdImageStore=$this->BannerAdImageReopsitories->store($data);
        DB::commit();
        session()->flash('flash_message_success','Image Store Successfuly!!');
        return \redirect()->route('admin.banner-ad-images.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $subCategoryList=$this->BannerAdImageReopsitories->subCategoryList();

        $bannerAdImage=$this->BannerAdImageReopsitories->findById($id);
        return \view('admin.pages.banner-ad-images.edit')
                    ->with('subCategoryLists', $subCategoryList)
                    ->with('bannerAdImages', $bannerAdImage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
              
        $validator = Validator::make($request->all(),(new BannerAdImageRule($request))->getBannerAdImageRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $updateBannerImages=$this->BannerAdImageReopsitories->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.banner-ad-images.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
