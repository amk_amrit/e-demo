<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderPayment;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{
    public function __construct(){
        $this->middleware('Admin');

}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $order=Order::with('orderProduct')->get();
        return \view('admin.pages.order.index')->with('orders', $order);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $orderList=Order::find($id);
        return \view('admin.pages.order.edit')->with("order_list", $orderList);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::beginTransaction();
        try{
        $updateStatus=Order::find($id);
        $updateStatus->status=$request->status;
        $updateStatus->save();
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.orders.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function paymentDetail($id){

        // $paymentDetails=OrderPayment::where()
    }
}
