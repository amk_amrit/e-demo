<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductSubCategory\ProductSubCategoryInterface;
use App\ValidationRules\ProductSubCategoryRule;
use Illuminate\Support\Facades\Validator;
use DB;

class SubCategoriesController extends Controller
{
    private $productSubCategoryReopsitories;

    public function __construct(ProductSubCategoryInterface $productSubCategoryReopsitories){
            $this->productSubCategoryReopsitories=$productSubCategoryReopsitories;
            $this->middleware('Admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productSubCategory=$this->productSubCategoryReopsitories->all();
        return view('admin.pages.product.sub-categories.index')->with('productSubCategories', $productSubCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $productCategory=$this->productSubCategoryReopsitories->allCategory();
        return view('admin.pages.product.sub-categories.create')->with('productCategories', $productCategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new ProductSubCategoryRule($request))->getProductSubCategoryRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $storeProductSubCategory=$this->productSubCategoryReopsitories->store($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.product.sub-categories.index');
        }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productCategory=$this->productSubCategoryReopsitories->allCategory();
        $productSubCategory=$this->productSubCategoryReopsitories->findById($id);
         //return $productCategory;
        return \view('admin.pages.product.sub-categories.edit')
        ->with('productCategories', $productCategory)
        ->with('productSubCategory', $productSubCategory);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new ProductSubCategoryRule($request))->getProductSubCategoryRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $updateProductSubCategory=$this->productSubCategoryReopsitories->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.product.sub-categories.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
