<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Repositories\Product\ProductInterface;
use App\ValidationRules\ProductImageRule;
use Illuminate\Support\Facades\Validator;
use DB;

class ProductImageController extends Controller
{
    private $productReopsitories;

    public function __construct(ProductInterface $productReopsitories){
            $this->productReopsitories=$productReopsitories;
            $this->middleware('Admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new ProductImageRule($request))->getProductImageRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
       
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $productImage=$this->productReopsitories->productImageUpdate($data);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.products.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $productAttribute=$this->productReopsitories->productAttribute($id);
        $productImage=$this->productReopsitories->productImage($id);
        return \view('admin.pages.product.product-images.show')
                        ->with('productAttributes', $productAttribute)
                        ->with('productImages', $productImage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productAttribute=$this->productReopsitories->productAttributeList($id);
        $productImage=$this->productReopsitories->productImageList($id);
        return \view('admin.pages.product.product-images.edit')
                ->with('productAttributes', $productAttribute)
                ->with('productImages' ,$productImage);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new ProductImageRule($request))->getProductImageRules('create'));
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $productImageStore=$this->productReopsitories->productImageStore($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.products.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $productImageDelete=$this->productReopsitories->productImageDelete($id);
    }
    public function productImageCreate($id){
        $productAttribute=$this->productReopsitories->productAttributeList($id);
        return view('admin.pages.product.product-images.create')
        ->with('productAttributes',$productAttribute)
        ->with('productId', $id);
    }
}
