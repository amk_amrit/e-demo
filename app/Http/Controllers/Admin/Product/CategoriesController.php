<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Repositories\ProductCategory\ProductCategoryInterface;
use App\ValidationRules\ProductCategoryRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use DB;

class CategoriesController extends Controller
{
    private $productCategoryReopsitories;

    public function __construct(ProductCategoryInterface $productCategoryReopsitories){
            $this->productCategoryReopsitories=$productCategoryReopsitories;
            $this->middleware('Admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productCategory=$this->productCategoryReopsitories->all();
        return view('admin.pages.product.categories.index')->with('productCategories', $productCategory);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.product.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new ProductCategoryRule($request))->getProductCategoryRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->except('_token');
        $productCategoryStore=$this->productCategoryReopsitories->store($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.product.categories.index');
        }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productCategory=$this->productCategoryReopsitories->findById($id);
        return \view('admin.pages.product.categories.edit')->with('productCategories', $productCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new ProductCategoryRule($request))->getProductCategoryRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $updateProductCategory=$this->productCategoryReopsitories->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return \redirect()->route('admin.product.categories.index');
    }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
