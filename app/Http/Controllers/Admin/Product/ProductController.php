<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Product\ProductInterface;
use App\ValidationRules\ProductRule;
use Illuminate\Support\Facades\Validator;
use DB;

class ProductController extends Controller
{
    
    private $productReopsitories;

    public function __construct(ProductInterface $productReopsitories){
            $this->productReopsitories=$productReopsitories;
            $this->middleware('Admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product=$this->productReopsitories->all();
        return \view('admin.pages.product.products.index')->with('products', $product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $productCategory=$this->productReopsitories->productCategory();
        $productSubCategory=$this->productReopsitories->productSubCategory();
        return \view('admin.pages.product.products.create')
                    ->with('productCategories', $productCategory)
                    ->with('productSubCategories', $productSubCategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),(new ProductRule($request))->getProductRules('create'));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $storeProduct=$this->productReopsitories->store($data);
        DB::commit();
        session()->flash('flash_message_success','Data Store Successfuly!!');
        return redirect()->route('admin.products.index');
        }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product=$this->productReopsitories->findById($id);
        $productAttribute=$this->productReopsitories->productAttribute($id);
        $productImage=$this->productReopsitories->productImage($id);
        return \view('admin.pages.product.products.show')
                        ->with('productAttributes', $productAttribute)
                        ->with('productImages', $productImage)
                        ->with('products', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productCategory=$this->productReopsitories->productCategory();
        $productSubCategory=$this->productReopsitories->productSubCategory();
        $products=$this->productReopsitories->findById($id);
        return \view('admin.pages.product.products.edit')
                    ->with('productCategories', $productCategory)
                    ->with('productSubCategories' , $productSubCategory)
                    ->with('product', $products);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),(new ProductRule($request))->getProductRules('update', $id));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        //
        DB::beginTransaction();
        try{
        $data=$request->all();
        $updateProduct=$this->productReopsitories->update($data, $id);
        DB::commit();
        session()->flash('flash_message_success','Data Update Successfuly!!');
        return redirect()->route('admin.products.index');
        }catch (\Exception $exception){
        DB::rollback();
        return redirect()->back()
            ->withInput()
            ->with('flash_message_error', 'Something went wrong ' . $exception->getMessage().'.');
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function productAttributeList($productId){
        $productAttribute=$this->productReopsitories->productAttributeList($productId);
        return $productAttribute;
    }
}
