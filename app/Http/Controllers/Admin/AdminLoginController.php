<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;

class AdminLoginController extends Controller
{
    //
    public function index(){
        if(empty(Session::get('user_names'))){
            return view('admin.pages.login.index');
        }
        else{
            return view('admin.index'); 
        }
       
    }
    public function login(Request $request){
        
        //validate the form data
    	$this->validate($request,[
    		'email'=>'required|email',
    		'password'=>'required|min:6'
    	]);
    //Attem Login
    	if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request ->password], $request->remember)) {
    		// if success then redirect to their intended location
            Session()->put('user_names', $request->email);
            return view('admin.index');    	
        }
    	//if unsuccess then back login form
    	return redirect()->back()->withInput($request->only('email','remember'));
    }
    
   public function logout(){
       Session::flush();
    return view('admin.pages.login.index');

   }
}
