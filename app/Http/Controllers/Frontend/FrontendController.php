<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\BannerAdImage;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductSubCategory;

use App\User;
use DateTime;
use DB;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    //
    public function index(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $sliderList=BannerAdImage::where('image_types', 'slider_image')->get();
        $latestProductList=Product::where('status', 1)->paginate(8);
        $topSellingProductList=Product::where('status', 1)->paginate(8);
        $BannerImageList=BannerAdImage::where('image_types', 'banner_ad_image')->get();
        $companyInformation=User::where('user_types', 'admin')->first();
         //return $latestProductList;
        return view('frontend.index')
                ->with('categoriesLists',$categoriesList)
                ->with('subCategoriesLists',$subCategoriesList)
                ->with('sliderLists',$sliderList)
                ->with('latestProductLists',$latestProductList)
                ->with('topSellingProductLists',$topSellingProductList)
                ->with('BannerImageLists',$BannerImageList)
                ->with('companyInformations',$companyInformation);
    }
    public function getCategoryData($id){
        $getProductList = Product::where('status', 1)
                                        ->where('product_category_id', $id)
                                        ->paginate(12);
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();

            return view('frontend.pages.category')
                ->with('productLists',$getProductList )
                ->with('categoriesLists',$categoriesList)
                ->with('subCategoriesLists',$subCategoriesList)
                ->with('companyInformations',$companyInformation);
    }
    public function getSubCategoryData($id){
        $getProductList = Product::where('status', 1)
                                    ->where('product_sub_category_id', $id)
                                    ->paginate(12);
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();

        return view('frontend.pages.category')
        ->with('productLists',$getProductList )
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList)
        ->with('companyInformations',$companyInformation);       
     }
public function getSingleProduct($id){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();
        $getProductDetails=Product::find($id);
        $getProductImages=ProductImage::where('product_id', $id)->get();
        $getProductAttribute=ProductAttribute::where('product_id', $id)->get();
        // return $getProductDetails;
        return view('frontend.pages.single-product')
        ->with('productImages', $getProductImages)
        ->with('productAttributs', $getProductAttribute)
        ->with('productDetails', $getProductDetails)
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList)
        ->with('companyInformations',$companyInformation); 
    }
    public function aboutUs(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();
        return \view('frontend.pages.about-us')
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList)
        ->with('companyInformations',$companyInformation); 
    }
    public function contactUs(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();
        return \view('frontend.pages.contact-us')
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList)
        ->with('companyInformations',$companyInformation); 
    }
    public function getBlog(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        $companyInformation=User::where('user_types', 'admin')->first();
        return \view('frontend.pages.blog')
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList)
        ->with('companyInformations',$companyInformation); 
    }
}