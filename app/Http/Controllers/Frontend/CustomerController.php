<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Models\ProductSubCategory;
use App\User;
use Auth;
use Hash;

class CustomerController extends Controller
{
    //
    public function getLoginForm(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();

        return \view('frontend.pages.login')
                ->with('categoriesLists',$categoriesList)
                ->with('subCategoriesLists',$subCategoriesList);
    }
    public function getSignUpForm(){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();

        if(empty(Session::get('customer_name'))){
            return view('frontend.pages.customer-dastboard')
            ->with('categoriesLists',$categoriesList)
            ->with('subCategoriesLists',$subCategoriesList);
        }
        else{
            return view('admin.index'); 
        }
        
        return \view('frontend.pages.register')
                ->with('categoriesLists',$categoriesList)
                ->with('subCategoriesLists',$subCategoriesList);
    }
    public function login( Request $request){
        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();
        //validate the form data
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);
        //Attem Login
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request ->password, ], $request->remember)) {
            // if success then redirect to their intended location
            Session()->put('customer_name', $request->email);
            return view('frontend.pages.customer-dastboard')
                ->with('categoriesLists',$categoriesList)
                ->with('subCategoriesLists',$subCategoriesList);    	
        }
        //if unsuccess then back login form
        return redirect()->back()->withInput($request->only('email','remember'));
        }   
    public function SignUp(Request $request){

        $categoriesList=ProductCategory::where('status', 1)->get();
        $subCategoriesList=ProductSubCategory::where('status',1)->get();

        $users= new User;
        $users->name=$request->name;
        $users->email=$request->email;
        $users->address=$request->address;
        $users->facebook_id=$request->facebook_id;
        $users->google_id=$request->google_id;
        $users->phone=$request->phone;
        $users->mobile_number=$request->mobile_number;
        $users->user_types='customer';
        $password=$request->password;
        $hasPassword=bcrypt($password);
        $users->password=$hasPassword;
        $users->save();
        Session()->put('customer_name', $request->email);
        return view('frontend.pages.customer-dastboard')
        ->with('categoriesLists',$categoriesList)
        ->with('subCategoriesLists',$subCategoriesList);  
    }
    
}
