<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');        
           });

        Schema::table('product_sub_categories', function (Blueprint $table) {
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });

        Schema::table('products', function (Blueprint $table) {
             $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('no action');
             $table->foreign('product_sub_category_id')->references('id')->on('product_sub_categories')->onDelete('no action');
             $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
             $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });

        Schema::table('product_attributes', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });
       Schema::table('product_images', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('no action');         
            $table->foreign('product_attribute_id')->references('id')->on('product_attributes')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });
        Schema::table('term_and_conditions', function (Blueprint $table) {
             $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
             $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });
        Schema::table('banner_ad_images', function (Blueprint $table) {
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
       });
       Schema::table('user_payment_card_informations', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('no action');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
        });
        Schema::table('orders', function (Blueprint $table) {
             $table->foreign('order_user_id')->references('id')->on('users')->onDelete('no action');
             $table->foreign('product_name')->references('id')->on('products')->onDelete('no action');
        });
        Schema::table('order_payments', function (Blueprint $table) {
             $table->foreign('order_user_id')->references('id')->on('users')->onDelete('no action');
             $table->foreign('approved_by')->references('id')->on('users')->onDelete('no action');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
