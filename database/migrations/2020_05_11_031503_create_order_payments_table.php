<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_user_id')->unsigned();
            $table->float('payment_amount');
            $table->integer('one_time_order_id');
            $table->bigInteger('approved_by')->unsigned();
            $table->timestamps();
        });
         // $table->foreign('order_user_id')->references('id')->on('users')->onDelete('no action');
          // $table->foreign('approved_by')->references('id')->on('users')->onDelete('no action');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_payments');
    }
}
