<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('user_image')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('status')->nullable()->default(0);
            $table->bigInteger('pan_number')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_number')->nullable();
            $table->enum('user_types',['admin','customer'])->nullable();
            $table->text('about_us')->nullable();
            $table->string('messanger_id')->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        // $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
         // $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
