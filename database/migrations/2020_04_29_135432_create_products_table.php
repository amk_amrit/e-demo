<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_category_id')->unsigned();
            $table->bigInteger('product_sub_category_id')->unsigned();
            $table->string('product_name');
            $table->string('thump_images');
            $table->float('main_price');
            $table->string('description');
            $table->boolean('status')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();
            $table->unique(['product_name'], 'uq_columns');

        });
         // $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('no action');
        // $table->foreign('product_sub_category_id')->references('id')->on('product_sub_categories')->onDelete('no action');
        // $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
        // $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
