<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_user_id')->unsigned();
            $table->bigInteger('product_name')->unsigned();
            $table->integer('product_quantity');
            $table->float('per_unit_price');
            $table->enum('status',['approved','panding','complet']);
            $table->integer('one_time_order_id');
            $table->timestamps();
        });
         // $table->foreign('order_user_id')->references('id')->on('users')->onDelete('no action');
        // $table->foreign('product_name')->references('id')->on('products')->onDelete('no action');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
