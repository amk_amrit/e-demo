<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerAdImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_ad_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('banner_add_name');
            $table->string('images');
            $table->integer('order')->nullable();
            $table->enum('image_types',['banner_ad_image','slider_image']);
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->bigInteger('sub_category_link_id ')->nullable();
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();
            $table->unique(['order'], 'uq_columns');

        });
        // $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
         // $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_add_images');
    }
}
