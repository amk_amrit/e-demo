<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sub_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_sub_category_name');
            $table->bigInteger('product_category_id')->unsigned();
            $table->string('description');
            $table->boolean('status')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();
            $table->timestamps();
            $table->unique(['product_sub_category_name', 'product_category_id'], 'uq_columns');
        });
        // $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('no action');
        // $table->foreign('created_by')->references('id')->on('users')->onDelete('no action');
        // $table->foreign('updated_by')->references('id')->on('users')->onDelete('no action');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sub_categories');
    }
}
